from sanic import Sanic, Request
from sanic.response import json, text, HTTPResponse

from database import db_settings
from settings import MyConfig, NanoSecondRequest

app = Sanic("WebSocketApp", config=MyConfig(), request_class=NanoSecondRequest)

app.config.update(db_settings)


@app.route('/')
async def test(request):
    return json({'hello': 'world'})


@app.route('/test')
async def test2(request):
    return text("dasdasd")


@app.route('/tt')
async def type_haldler(request: Request) -> HTTPResponse:
    return text("Done.")


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000)
