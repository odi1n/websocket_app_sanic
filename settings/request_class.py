import time

from sanic import Request


class NanoSecondRequest(Request):
    @classmethod
    def generate_id(*_):
        return time.time_ns()